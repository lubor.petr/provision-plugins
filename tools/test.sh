#!/usr/bin/env bash

export GOOS="$(go env GOOS)"
export GOARCH="$(go env GOARCH)"

[[ -x $HOME/go/bin/govulncheck ]] || go install golang.org/x/vuln/cmd/govulncheck@latest
for bin in $PWD/bin/$GOOS/$GOARCH/*; do
  govulncheck -mode=binary -json "$bin" || exit 1
done
