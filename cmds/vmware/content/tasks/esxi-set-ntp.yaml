---
Name: "esxi-set-ntp"
Description: "Set the NTP system in ESXi"
Documentation: |
  **For ESXi 7.x systems:**

  Set the NTP system in ESXi based the following params, evaulated
  in the order:

    * `ntp-servers`

  This task will also appropriately modify the ESXi Firewall rulesets
  to allow access to the NTP servers based on the `ntpClient` ruleset.

  The following expected behaviors will occur:

    * setting `esxi/ntp-conf` will generate a Task error and stop workflow processing
    * if no `ntp-servers` value is set, NTP will not be changed/configured
    * if any `ntp-servers` setting exists, then a reset of the NTP configuration is performed
    * if the NTP servers are host/domain names, they MUST correctly resolve in DNS, otherwise a Task error will result, and the the workflow processing will stop (this is based on how VMware defines the behavior)
    * if the NTP servers are IP addresses, then the service will be configured without any service validation of the IP address, this may result in non-functioning NTP services if all defined addresses are not accessible or providing NTP server serivces
    * if NTP was previously configured, and if there iare configured Servers in `ntp-servers` param and they re non DNS resolvable, the NTP service will result in being disabled and no longer functioning

  !!! warning
      **DEPRECATED:** the `esxi/ntp-conf` feature has been deprecated
      and no longer supported.  Using it will generate an error and stop
      Workflow processing.  Set only the `ntp-servers` list to specify
      time services on an ESXi host.  No additional customization is
      supported by VMware.

  **For ESXi 6.x systems:**

  Set the NTP system in ESXi 6.x versions based the following params, evaulated
  in the order:

      ntp-servers

  This task will also appropriately modify the ESXi Firewall rulesets to allow
  access to the NTP servers based on the ntpClient ruleset.  Full customization
  of the ntp.conf file can override the generated config files from this task
  by using the `esxi/ntp-conf` param.

OptionalParams:
  - ntp-servers
  - esxi/ntp-conf
  - rs-debug-enable
Meta:
  icon: "cloud"
  color: "yellow"
  title: "Digital Rebar"
Templates:
  - Name: "esxi-set-ntp-via_esxcli.sh"
    ID: "esxi-set-ntp-via_esxcli.sh.tmpl"
    Path: "/tmp/esxi-set-ntp-via_esxcli.sh"
  - Name: "esxi-set-ntp-via_ntpconf.sh"
    ID: "esxi-set-ntp-via_ntpconf.sh.tmpl"
    Path: "/tmp/esxi-set-ntp-via_ntpconf.sh"
  - Name: "esxi-set-ntp.sh"
    Contents: |
      #!/usr/bin/env sh
      # Configure ESXi NTP time handling services

      {{ if eq (.Param "rs-debug-enable") true }}set -x{{ end }}
      set -e

      echo ""

      # we can't rely on the BootEnv Version field being correct, unfortunately....
      VER=$(esxcli system version get | grep Version | awk ' { print $NF } ')

      case $VER in
        5*|6*)
          echo ">>> Using ntp.conf based template for ESXi 5.x/6.x system (ver: '$VER')"
          /bin/sh /tmp/esxi-set-ntp-via_ntpconf.sh
        ;;
        *)
          echo ">>> Using 'esxcli' NTP template for ESXi 7.x system (ver: '$VER')"
          if esxcli system | grep -i ntp
          then
              /bin/sh /tmp//esxi-set-ntp-via_esxcli.sh
          else
            echo "FATAL: Unsupported ESXi version '$VER' or 'esxcli system ntp' commands"
            echo "       not supported..  Reported system information:"
            esxcli system version get
            exit 1
          fi
        ;;
      esac

      echo "Removing NTP modification templates"
      rm -f /tmp/esxi-set-ntp-via_ntpconf.sh /tmp/esxi-set-ntp-via_esxcli.sh

      echo ""
