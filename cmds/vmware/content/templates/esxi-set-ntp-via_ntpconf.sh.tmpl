#!/usr/bin/env sh
# DEPRECATED: Configure ESXi NTP time handling services via /etc/ntp.conf

### This content is only for older ESXi systems (eg 6.x) - ESXi 7.x systems
### must use the 'esxcli' configuration method - VMware uses a central
### config store that will overwrite ntp.conf settings.
###
### The Template named esxi-set-ntp-via_esxcli.sh.tmpl can accomplish the
### changes correctly for ESXi 7.x systems via the 'esxcli' command.

{{ if eq (.Param "rs-debug-enable") true }}set -x{{ end }}

{{ if .ParamExists "esxi/ntp-conf" -}}

{{ $tmpl := (.Param "esxi/ntp-conf") -}}
echo "Setting NTP config from template '{{ .Param "esxi/ntp-conf" }}'"
cat >/etc/ntp.conf <<NTPCONFIG
{{ .CallTemplate $tmpl .}}
NTPCONFIG
TWIDDLE_NTP="yes"

{{ else -}}

{{ if .ParamExists "ntp-servers" -}}

echo "Setting NTP from built in tooling..."
cat >/etc/ntp.conf <<NTPCONFIG
# installed by Digital Rebar Provision during provisioniing...
# to completely customize NTP settings, see the 'esxi/ntp-conf'
# param to provide your own NTP config template
restrict default kod nomodify notrap noquerynopeer
restrict 127.0.0.1

{{range $key, $ntp := .Param "ntp-servers" -}}
server {{ $ntp }} iburst
{{ end -}}
NTPCONFIG

TWIDDLE_NTP="yes"

{{ end -}}
{{ end -}}

if [[ -n "$TWIDDLE_NTP" ]]
then
  echo "Setting and starting 'ntpd' sevices to be on"
  localcli network firewall ruleset set --ruleset-id ntpClient --enabled true
  /sbin/chkconfig ntpd on
  /etc/init.d/ntpd restart
  # restart hostd for Web UI to reflect changes correctly
  /etc/init.d/hostd restart
  echo "Collecting NTP connection peer status..."
  sleep 30
  ntpq -np
  echo "Done."
fi

