package main

import (
	"encoding/binary"
	"fmt"
	"net"
)

const IPRangeLimit = 256

func validateIpRange(startIPStr, endIPStr string) (err error) {
	startIP := net.ParseIP(startIPStr).To4()
	endIP := net.ParseIP(endIPStr).To4()

	if startIP == nil || endIP == nil {
		err = fmt.Errorf("invalid IP address format for startIp: %s or endIp: %s", startIPStr, endIPStr)
		return
	}

	startIPInt := ipToInt(startIP)
	endIPInt := ipToInt(endIP)

	if startIPInt > endIPInt {
		err = fmt.Errorf("start IP %s is greater than end IP %s", startIPStr, endIPStr)
		return
	}

	rangeSize := int(endIPInt - startIPInt + 1)

	if rangeSize > IPRangeLimit {
		err = fmt.Errorf("the IP range cannot exceed %v addresses. range received: %v", IPRangeLimit, rangeSize)
		return
	}

	return
}

func ipToInt(ip net.IP) uint32 {
	return binary.BigEndian.Uint32(ip)
}

func intToIP(ipInt uint32) net.IP {
	ip := make(net.IP, 4)
	binary.BigEndian.PutUint32(ip, ipInt)
	return ip
}

func incrementIPAddress(ip string, index int) string {
	if index == -1 {
		return ""
	}
	parsedIP := net.ParseIP(ip)
	if parsedIP == nil {
		return ""
	}

	ipAddress := parsedIP.To4()
	if ipAddress == nil {
		return ""
	}

	newAddress := intToIP(ipToInt(ipAddress) + uint32(index))
	return newAddress.String()
}

func removeEmptyStrings(slice []string) []string {
	var filteredSlice []string
	for _, str := range slice {
		if str != "" {
			filteredSlice = append(filteredSlice, str)
		}
	}
	return filteredSlice
}

func createInitialScanStateSlice(rangeSize, lowestIndex int) []bool {
	slice := make([]bool, rangeSize)

	for i := 0; i < rangeSize; i++ {
		if i <= lowestIndex {
			slice[i] = true
		} else {
			slice[i] = false
		}
	}
	return slice
}

func findLowestScannedIndex(res *IpmiScanResult) int {
	for i := res.LowestScannedIndex + 1; i < len(res.ipScanState); i++ {
		if !res.ipScanState[i] {
			return i - 1
		}
	}
	return len(res.ipScanState) - 1
}
