package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"gitlab.com/rackn/provision/v4/api"
	"gitlab.com/rackn/provision/v4/models"
	"log"
	"os"
	"strconv"
	"strings"
)

type auth struct {
	oob              bool
	addr, user, pass string
	machine          string
	job              string
	tftpServer       string
	client           *api.Client
}

func (a auth) oobOK() error {
	var missing []string
	if a.addr == "" {
		missing = append(missing, "Address")
	}
	if a.user == "" {
		missing = append(missing, "Username")
	}
	if a.pass == "" {
		missing = append(missing, "Password")
	}
	if a.job == "" {
		missing = append(missing, "Current Job")
	}
	if a.tftpServer == "" {
		missing = append(missing, "TFTP Server")
	}
	if !a.oob {
		missing = append(missing, "Out Of Band Param")
	}
	if len(missing) == 0 {
		return nil
	}
	return fmt.Errorf("Missing OOB requirements: %v", strings.Join(missing, ", "))
}

func (a auth) offline() bool {
	return a.client == nil
}

func main() {
	var driver, op, src, cfgSrc string
	var oobAuth auth
	var dryRun bool
	flag.StringVar(&driver, "driver", "", "Driver to use for BIOS configuration. One of dell hp lenovo none dell-legacy supermicro cisco")
	flag.StringVar(&op, "operation", "get", "Operation to perform, one of: get test apply export")
	flag.StringVar(&src, "source", "", "Source config file to read from for testing.  Can be left blank to use the current system config.  Must be in the native tooling format for the driver (racadm get --clone XML for Dell, conrep xml for HP, list for OneCli)")
	flag.StringVar(&cfgSrc, "config", "-", "Configuration to test or apply.  '-' means read from stdin.")
	flag.BoolVar(&dryRun, "dryRun", false, "Skip actually making changes when apply is the op.")
	// OOB params.  Can be left blank, which will default to having the drivers attempt in-band config if possible.
	flag.StringVar(&oobAuth.addr, "address", "", "Remote address to contact for out-of-band configuration. If left unset, defaults to in-band operation")
	flag.StringVar(&oobAuth.user, "username", "", "Username for out-of-band configuration")
	flag.StringVar(&oobAuth.pass, "password", "", "Password for out-of-band configuration")
	flag.Parse()
	endpoint := os.Getenv("RS_ENDPOINT")
	token := os.Getenv("RS_TOKEN")
	key := os.Getenv("RS_KEY")
	username, pass, _ := strings.Cut(key, ":")
	oobAuth.machine = os.Getenv("RS_UUID")
	oobAuth.job = os.Getenv("RS_JOB_UUID")
	oobAuth.tftpServer = os.Getenv("TFTPSERVER")
	var err error
	if oobAuth.machine == "" {
		err = fmt.Errorf("RS_UUID not set")
	}
	if err == nil {
		if oobAuth.client, err = api.TokenSession(endpoint, token); err != nil {
			if oobAuth.client, err = api.UserSession(endpoint, username, pass); err != nil {
				log.Printf("Unable to contact dr-provision, operating in offline mode: %v", err)
			}
		}
	}
	var info *models.Info
	m := &models.Machine{}
	if err == nil {
		info, err = oobAuth.client.Info()
	}
	if err == nil && oobAuth.tftpServer == "" {
		oobAuth.tftpServer = info.Address.String() + ":" + strconv.Itoa(info.TftpPort)
	}
	if err == nil {
		err = oobAuth.client.Req().UrlFor("machines", oobAuth.machine).Do(m)
	}
	if err == nil && oobAuth.job == "" {
		oobAuth.job = m.CurrentJob.String()
	}
	if err == nil {
		err = oobAuth.client.Req().
			UrlFor("machines", oobAuth.machine, "params", "bios-out-of-band").
			Params("aggregate", "true").Do(&oobAuth.oob)
	}
	if err == nil && oobAuth.addr == "" {
		err = oobAuth.client.Req().
			UrlFor("machines", oobAuth.machine, "params", "ipmi/address").
			Params("aggregate", "true").Do(&oobAuth.addr)

	}
	if err == nil && oobAuth.user == "" {
		err = oobAuth.client.Req().
			UrlFor("machines", oobAuth.machine, "params", "ipmi/username").
			Params("aggregate", "true").Do(&oobAuth.user)

	}
	if err == nil && oobAuth.pass == "" {
		err = oobAuth.client.Req().
			UrlFor("machines", oobAuth.machine, "params", "ipmi/password").
			Params("aggregate", "true", "decode", "true").
			Do(&oobAuth.pass)
	}
	if err != nil {
		log.Printf("Operating in offline mode: %v", err)
		if oobAuth.client != nil {
			oobAuth.client.Close()
			oobAuth.client = nil
		}
		err = nil
	}
	var cfg Configurator
	switch driver {
	case "dell-legacy":
		cfg = &dellBiosOnlyConfig{auth: oobAuth}
	case "dell":
		cfg = &dellRacadmConfig{auth: oobAuth}
	case "hp":
		cfg = &hpConfig{auth: oobAuth}
	case "lenovo":
		cfg = &lenovoConfig{auth: oobAuth}
	case "supermicro":
		cfg = &superMicroConfig{auth: oobAuth}
	case "cisco":
		cfg = &ciscoCimcConfig{auth: oobAuth}
	case "none":
		cfg = &noneConfig{auth: oobAuth}
	default:
		log.Fatalf("Unknown driver %s", driver)
	}

	if src != "" {
		toRead, err := os.Open(src)
		if err != nil {
			log.Fatalf("Unable to open source %s: %v", src, err)
		}
		defer toRead.Close()
		cfg.Source(toRead)
	}
	exitCode := 0
	var ents map[string]Entry
	needReboot := false
	vars := map[string]string{}
	var dec *json.Decoder
	if cfgSrc == "-" {
		dec = json.NewDecoder(os.Stdin)
	} else {
		fi, err := os.Open(cfgSrc)
		if err != nil {
			log.Fatalf("Error opening %s: %v", cfgSrc, err)
		}
		dec = json.NewDecoder(fi)
	}
	enc := json.NewEncoder(os.Stdout)
	enc.SetIndent(``, `  `)
	var res interface{}
	res = map[string]interface{}{}
	switch op {
	case "export":
		ents, err = cfg.Current()
		if err != nil {
			log.Fatalf("Error getting config: %v", err)
		}
		vals := map[string]string{}
		for k, v := range ents {
			vals[k] = v.Current
		}
		res = vals
	case "get":
		ents, err = cfg.Current()
		if err != nil {
			log.Fatalf("Error getting config: %v", err)
		}
		res = ents
	case "test":
		if err = dec.Decode(&vars); err != nil {
			log.Fatalf("Unable to parse JSON config on stdin: %v", err)
		}
		vars = cfg.FixWanted(vars)
		_, willAttempt, err := Test(cfg, vars)
		if err != nil {
			log.Fatalf("Error figuring out what would be applied: %v", err)
		}
		res = willAttempt
	case "apply":
		if err = dec.Decode(&vars); err != nil {
			log.Fatalf("Unable to parse JSON config on stdin: %v", err)
		}
		var trimmed map[string]string
		vars = cfg.FixWanted(vars)
		ents, trimmed, err = Test(cfg, vars)
		if err == nil && len(trimmed) != 0 {
			for k, v := range trimmed {
				log.Printf("Attempting to set %s to %s\n", k, v)
			}
			needReboot, err = cfg.Apply(ents, trimmed, dryRun)
			if needReboot {
				exitCode += 192
			}
		}
		if err != nil {
			exitCode += 1
		}
		res = trimmed
	default:
		log.Fatalf("Unknown op '%s'", op)
	}
	enc.Encode(res)
	if err != nil {
		log.Println(err)
	} else {
		log.Printf("Op %s succeeded", op)
	}
	os.Exit(exitCode)
}
