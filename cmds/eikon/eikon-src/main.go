package main

import (
	"flag"
	"fmt"
	"os"
)

var debug DebugLevel

func main() {
	filePtr := flag.String("file", "config.yaml", "The system configuration file")
	dumpPtr := flag.Bool("dump", false, "Dump config objects")
	debugPtr := flag.Int("debug", 0, "Debug level (number bitfield)")
	inventoryPtr := flag.Bool("inventory", false, "Dump Inventory")
	validatePtr := flag.Bool("validate", false, "Validateconfig objects")
	mountPtr := flag.String("mount", "", "Mount the config file at the directory")
	unmountPtr := flag.String("unmount", "", "Unmount the config file at the directory")

	flag.Parse()

	debug = DebugLevel(*debugPtr)

	c := &Config{}
	if e := c.ScanSystem(); e != nil {
		fmt.Printf("Error: scanning %v\n", e)
		os.Exit(1)
	}
	if *dumpPtr || *inventoryPtr {
		c.Dump()
	}
	if *inventoryPtr {
		os.Exit(0)
	}

	ic := &Config{}
	if e := ic.ReadConfig(*filePtr); e != nil {
		fmt.Printf("Error: parsing config file: %s :%v\n", *filePtr, e)
		os.Exit(1)
	}
	if *validatePtr {
		ic.Dump()
		os.Exit(0)
	}
	if *dumpPtr {
		ic.Dump()
	}

	if *unmountPtr != "" {
		if e := ic.ScanSystem(); e != nil {
			fmt.Printf("Error: scanning %v\n", e)
			os.Exit(1)
		}
		_, err := ic.UnmountAll(*unmountPtr)
		if err != nil {
			fmt.Printf("Error: Unmounting %s: %v\n", *unmountPtr, err)
			os.Exit(1)
		}
		os.Exit(0)
	}
	if *mountPtr != "" {
		if e := ic.ScanSystem(); e != nil {
			fmt.Printf("Error: scanning %v\n", e)
			os.Exit(1)
		}
		_, _, err := ic.MountAll(*mountPtr)
		if err != nil {
			fmt.Printf("Error: Mounting %s: %v\n", *mountPtr, err)
			os.Exit(1)
		}
		os.Exit(0)
	}

	rs, err := c.Apply(ic)
	if err != nil {
		fmt.Printf("Error Applying: %v\n", err)
		os.Exit(1)
	}
	for rs == ResultRescan {
		if e := c.ScanSystem(); e != nil {
			fmt.Printf("Error: scanning %v\n", e)
			os.Exit(1)
		}
		if *dumpPtr {
			c.Dump()
		}
		rs, err = c.Apply(ic)
		if err != nil {
			fmt.Printf("Error Applying: %v\n", err)
			os.Exit(1)
		}
	}

	os.Exit(0)
}
