package main

import (
	"fmt"
	"os/exec"
	"strconv"
	"strings"
)

// Disk defines a logical disk in the system.
//
// This could be a SSD, Spinning disk, or
// external raid drives.  It is basis for all
// other components.
//
// One Path must be specified in the config section.
type Disk struct {
	// Name of disk for future reference
	Name string `json:"name"`

	// Path specifies the path to disk
	Path string `json:"path,omitempty"`

	// Path specifies the path to disk by id
	PathByID string `json:"path_by_id,omitempty"`

	// Path specifies the path to disk by uuid
	PathByUUID string `json:"path_by_uuid,omitempty"`

	// Path specifies the path to disk by pci path
	PathByPath string `json:"path_by_path,omitempty"`

	// Image to apply to the disk.
	// Image needs to be a raw format (not tar/zip)
	Image *Image `json:"image,omitempty"`

	// Wipe indicates if the disk should be wiped first
	// meaning PVs, VGs, and FSes removed.
	//
	// Having an image object implies wipe
	Wipe bool `json:"wipe,omitempty"`

	// Zero indicates that the disk should also be zeroed.
	//
	// Having an image object implies zeros
	Zero bool `json:"zero,omitempty"`

	// Partition to use for this disk (if needed).
	// required: true
	PTable string `json:"ptable,omitempty"`

	// GrubDevice indicates if grub should be placed on this device.
	GrubDevice bool `json:"grub_device,omitempty"`

	// Partitions contain the sub-pieces of the disk
	Partitions Partitions `json:"partitions,omitempty"`

	// Size is the size of this disk in bytes
	// Derived value
	Size string `json:"size,omitempty"`

	// FileSystem if defined
	FileSystem *FileSystem `json:"fs,omitempty"`

	// has the image been applied
	imageApplied bool

	// has the label been applied
	labelApplied bool

	// The next starting point for the partition
	nextStart string

	wiped  bool
	zeroed bool
}

// Disks defines a list of disk
type Disks []*Disk

// toCompList converts a list of disks to a list of Comparators
func (ds Disks) toCompList() []Comparator {
	ns := []Comparator{}
	for _, d := range ds {
		if d != nil {
			ns = append(ns, d)
		}
	}
	return ns
}

// FindDiskByPath finds a disk by path
func (ds Disks) FindDiskByPath(path string) *Disk {
	for _, d := range ds {
		if d.Path == path {
			return d
		}
	}
	return nil
}

// FindByName finds a piece by name
func (ds Disks) FindByName(name string) interface{} {
	for _, d := range ds {
		if d.Name == name {
			return d
		}
		if obj := d.Partitions.FindByName(name); obj != nil {
			return obj
		}
	}
	return nil
}

// FindPartitionByPath finds a partition by path
func (ds Disks) FindPartitionByPath(path string) *Partition {
	for _, d := range ds {
		if p := d.Partitions.FindPartitionByPath(path); p != nil {
			return p
		}
	}
	return nil
}

// ToDiskList converts a list of comparators into a disk list.
// If an error is passed in, then return that error.
func ToDiskList(src []Comparator, err error) (Disks, error) {
	if err != nil {
		return nil, err
	}
	ds := Disks{}
	for _, s := range src {
		if s != nil {
			ds = append(ds, s.(*Disk))
		}
	}
	return ds, nil
}

// updateBlkInfo updates the disk with key/values from lsblk
func (d *Disk) updateBlkInfo(keys map[string]string) error {

	if fst, ok := keys["FSTYPE"]; ok && fst != "" {
		if d.FileSystem == nil {
			d.FileSystem = &FileSystem{}
		}
		fs := d.FileSystem
		fs.updateBlkInfo(keys)
	}

	return nil
}

// Equal compares to disks for identity equivalence.
// This is used to make sure two disks reference the same
// disk.  This is used for config to actual and actual to actual.
func (d *Disk) Equal(c Comparator) bool {
	nd := c.(*Disk)
	// GREG: this needs work to handle other path comparators.
	return d.Path == nd.Path
}

// Merge takes the incoming argument and replaces the actual parts.
func (d *Disk) Merge(c Comparator) error {
	nd := c.(*Disk)
	d.PTable = nd.PTable
	d.GrubDevice = nd.GrubDevice
	d.Size = nd.Size
	d.nextStart = nd.nextStart
	if d.FileSystem != nil {
		if err := d.FileSystem.Merge(nd.FileSystem); err != nil {
			return err
		}
	} else {
		d.FileSystem = nd.FileSystem
	}
	var err error
	d.Partitions, err = ToPartitionList(
		MergeList(d.Partitions.toCompList(), nd.Partitions.toCompList()))
	return err
}

// Validate validates a Disk object.
func (d *Disk) Validate() error {
	out := []string{}

	if d.Name == "" {
		out = append(out, "name must be specified")
	}
	if d.Path == "" {
		out = append(out, "path must be specified")
	}

	var err error
	if d.Size != "" {
		d.Size, err = sizeToBytes(d.Size)
		if err != nil {
			return fmt.Errorf("Disk %s has invalid size: %v", d.Name, err)
		}
	}

	switch d.PTable {
	case "aix", "amiga", "bsd", "dvh", "gpt", "mac", "msdos", "pc98", "sun", "loop":
	default:
		out = append(out, fmt.Sprintf("ptable is not valid: %s", d.PTable))
	}

	for i, p := range d.Partitions {
		// If partition id is not specified, then build it from list position.
		if p.ID == 0 {
			p.ID = i + 1 // 1-based not 0-based
		}
		if e := p.Validate(); e != nil {
			out = append(out, fmt.Sprintf("disk %s: %s is invalid: %v", d.Name, p.Name, e))
		}
	}

	ids := map[int]struct{}{}
	for _, p := range d.Partitions {
		if _, ok := ids[p.ID]; ok {
			out = append(out, fmt.Sprintf("partitions using duplicate IDs on disk %s", d.Name))
		}
		ids[p.ID] = struct{}{}
	}

	if d.FileSystem != nil {
		if e := d.FileSystem.Validate(); e != nil {
			out = append(out, fmt.Sprintf("disk %s: FileSystem is invalid: %v", d.Name, e))
		}
	}

	if d.Image != nil {
		d.Image.rawOnly = true
		if e := d.Image.Validate(); e != nil {
			out = append(out, fmt.Sprintf("disk %s: Image is invalid: %v", d.Name, e))
		}
	}

	if len(out) > 0 {
		return fmt.Errorf(strings.Join(out, "\n"))
	}
	return nil
}

func (d *Disk) Remove() error {
	dmsg(DbgDisk|DbgRemove, "Wiping disk: (%s)\n", d.Name)

	for i := len(d.Partitions) - 1; i >= 0; i-- {
		p := d.Partitions[i]
		if err := p.Remove(); err != nil {
			return err
		}
	}

	if _, err := runCommand("wipefs", "--all", d.Path); err != nil {
		return err
	}

	return nil
}

func (d *Disk) doZero() error {
	dmsg(DbgDisk, "Zeroing disk: (%s) - TO DO\n", d.Name)
	// GREG: Zero disk
	return nil
}

// Action takes a config version of the object and applies it
// to the actual object.  return ResultRescan when a
// re-evaluation is needed.
func (d *Disk) Action(c Comparator) (Result, error) {
	di := c.(*Disk)
	dmsg(DbgDisk|DbgAction, "Disk (%s,%s) action\n", d.Name, di.Name)
	if !d.imageApplied {
		if (di.Wipe || di.Zero || di.Image != nil) && !d.wiped {
			if err := d.Remove(); err != nil {
				return ResultFailed, err
			}
			d.wiped = true
			return ResultRescan, nil
		}

		if di.Zero && di.Image == nil && !d.zeroed {
			if err := d.doZero(); err != nil {
				return ResultFailed, err
			}
			d.zeroed = true
			return ResultRescan, nil
		}

		if di.Image != nil {
			// Put image in place
			di.Image.Path = d.Path
			if err := di.Image.Action(); err != nil {
				return ResultFailed, err
			}
			d.imageApplied = true
			return ResultRescan, nil
		}
	}

	if !d.labelApplied && di.PTable != d.PTable {
		d.labelApplied = true
		return ResultRescan, runParted(d.Path, "mklabel", di.PTable)
	}

	// Ensure partition table is correct.
	if di.PTable != d.PTable {
		return ResultFailed, fmt.Errorf("Disk, %s, disk label doesn't match (A: %s C: %s)", di.Name, d.PTable, di.PTable)
	}

	// Build the partitions.
	dmsg(DbgDisk, "Building the partitions.\n")
	dmsg(DbgDisk, "Total partitions: %v\n", len(di.Partitions))
	for i, pi := range di.Partitions {
		dmsg(DbgDisk, "Working on %d partition: %v\n", i, pi)
		found := false
		for _, p := range d.Partitions {
			if p.Equal(pi) {
				r, e := p.Action(pi)
				if e != nil || r == ResultRescan {
					return r, e
				}
				found = true
				break
			}
		}
		if !found {
			// We just recreated a partition, rescan.
			// This is overkill but safe.
			p := &Partition{parent: d}
			r, e := p.Action(pi)
			if e != nil {
				return r, e
			}
			return ResultRescan, nil
		}
	}

	return ResultSuccess, nil
}

// ScanDisks scans the system for disks and partitions.
func ScanDisks() (Disks, error) {

	// Need to get a disk on the system and then run the better command
	out, err := exec.Command("parted", "-s", "-m", "--list").CombinedOutput()
	if err != nil {
		return nil, err
	}
	lines := strings.Split(string(out), "\n")
	nextLine := false
	disk := "/dev/cow"
	for _, line := range lines {
		line = strings.TrimSpace(line)
		if line == "" {
			continue
		}
		if nextLine {
			dmsg(DbgDisk|DbgScan, "processing find disk line: %s\n", line)
			parts := strings.Split(line, ":")
			disk = parts[0]
			break
		}
		if strings.Contains(line, "BYT;") {
			nextLine = true
		}
	}

	out, err = exec.Command("parted", "-s", "-m", disk, "unit", "b", "print", "list").CombinedOutput()
	if err != nil {
		return nil, err
	}

	disks := Disks{}

	lines = strings.Split(string(out), "\n")
	var currentDisk *Disk
	diskLine := false
	partLine := false
	partCount := 0
	diskCount := 0
	for _, line := range lines {
		line = strings.TrimSpace(line)
		if line == "" {
			diskLine = false
			partLine = false
			continue
		}
		if strings.HasPrefix(line, "Error") {
			dmsg(DbgDisk|DbgScan, "Skipping Error line: '%s'\n", line)
			continue
		}
		if strings.Contains(line, "BYT;") {
			currentDisk = &Disk{nextStart: "1048576B"}
			currentDisk.Name = fmt.Sprintf("disk-%d", diskCount)
			disks = append(disks, currentDisk)
			diskLine = true
			partLine = false
			continue
		}
		if diskLine {
			dmsg(DbgDisk|DbgScan, "processing disk line: %s\n", line)
			parts := strings.Split(line, ":")
			currentDisk.Path = parts[0]
			currentDisk.Size = parts[1]
			currentDisk.PTable = parts[5]
			partLine = true
			diskLine = false
			diskCount++
			continue
		}
		if partLine {
			dmsg(DbgDisk|DbgScan, "processing part line: %s\n", line)
			part := &Partition{}
			parts := strings.Split(line, ":")

			part.ID, _ = strconv.Atoi(parts[0])
			part.Start = parts[1]
			part.End = parts[2]
			part.Size = parts[3]
			part.PType = parts[4]
			part.Name = parts[5]
			if part.Name == "" {
				part.Name = fmt.Sprintf("part-%d", partCount)
			}
			if parts[6] != ";" && parts[6] != "" {
				part.Flags = parts[6]
			}
			part.parent = currentDisk
			currentDisk.Partitions = append(currentDisk.Partitions, part)
			end, _ := sizeParser(part.End)
			currentDisk.nextStart = sizeStringer(end+1, "B")
			partCount++
			continue
		}
	}

	return disks, nil
}

func (d *Disk) String() string {
	if d == nil {
		return "unset"
	}
	ps := ""
	comma := ""
	for _, p := range d.Partitions {
		ps += fmt.Sprintf("%s%v", comma, p)
		comma = ","
	}
	return fmt.Sprintf("<name:%s path:%s pathbyid:%s pathbyuuid:%s pathbypath:%s iamge:%v wipe:%v zero:%v ptable:%s grubdev:%v parts:%s size:%s fs:%v imageapplied:%v labelapplied:%v nextStart:%s>",
		d.Name,
		d.Path,
		d.PathByID,
		d.PathByUUID,
		d.PathByPath,
		d.Image,
		d.Wipe,
		d.Zero,
		d.PTable,
		d.GrubDevice,
		ps,
		d.Size,
		d.FileSystem,
		d.imageApplied,
		d.labelApplied,
		d.nextStart)
}
