package main

/*
  Name: app-optics-trigger-alert-webhook
  Description: Alert
  Documentation: Alert
  Path: kaholo-trigger-appoptics/src/config.json

const minimatch = require("minimatch");

async function alertWebhook(req, res, settings, triggerControllers) {
  try {
    const payload = JSON.parse(req.body.payload);
    const alertName = payload.alert.name;
    if (!alertName){
      return res.status(400).send("Bad AppOptics Webhook format");
    }
    const isCleared = payload.clear === "normal";
    triggerControllers.forEach(trigger => {
      const {alertNamePat, state} = trigger.params;

      if (alertNamePat && !minimatch(alertName, alertNamePat)) return;
      if (state === (isCleared ? "Active" : "Cleared")) return;

      const msg = `${alertName} ${isCleared ? "Cleared" : "Active"}`;
      trigger.execute(msg, payload);
    });
    res.status(200).send("OK");
  }
  catch (err){
    res.status(422).send(err.message);
  }
}

module.exports = {
  alertWebhook
};

*/

import (
	"encoding/json"
	"fmt"
	"regexp"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

type appOpticsTriggerData struct {
	Payload string `json:"payload"` // JSON Payload of the alert
}
type appOpticsPayload struct {
	Alert   appOpticsTriggerAlert `json:"alert"`
	Cleared string                `json:"cleared"`
}
type appOpticsTriggerAlert struct {
	Name string `json:"name"`
}

func app_optics_trigger_alert_webhook(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	reqData := &appOpticsTriggerData{}
	jerr := json.Unmarshal(triggerInput.Data, &reqData)
	if jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}
	payload := &appOpticsPayload{}
	jerr = json.Unmarshal([]byte(reqData.Payload), &payload)
	if jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR2: %v", jerr))
		return triggerResult, nil
	}
	if payload.Alert.Name == "" {
		triggerResult.Code = 400
		triggerResult.Data = []byte("Bad alert format: missing name")
		return triggerResult, nil
	}
	isCleared := payload.Cleared == "normal"

	count := 0
	for _, t := range triggerInput.Triggers {
		params, err := p.GetTriggerParams(t.Name)
		if err != nil {
			l.Errorf("Error getting %s trigger params: %v", err)
			continue
		}
		if anp, ok := params["app-optics-trigger/alert-webhook/alert-name-pat"]; ok {
			pat := anp.(string)
			if r, rerr := regexp.Compile(pat); rerr == nil {
				if !r.MatchString(payload.Alert.Name) {
					continue
				}
			} else {
				l.Errorf("Trigger %s has bad alert pattern: %v", rerr)
			}
		}
		if sobj, ok := params["app-optics-trigger/alert-webhook/state"]; ok {
			state := sobj.(string)
			if state != "Both" && ((isCleared && state == "Active") || (!isCleared && state == "Cleared")) {
				continue
			}
		}

		count++
		plugin.Publish("trigger", "fired", t.Name, payload)
	}
	triggerResult.HasCount = true
	triggerResult.Count = count
	return triggerResult, nil
}
