package main

/*
  Name: bitbucket-trigger-webhook-pr
  Description: Bitbucket pull request
  Documentation: Bitbucket pull request
  Path: kaholo-trigger-bitbucket/src/config.json

const minimatch = require("minimatch");

async function webhookPR(req, res, settings, triggerControllers) {
    const body = req.body;
    if (!body.repository) {
      return res.status(400).send(`Repository not found!`);
    }
    try {
    const reqRepoName = body.repository.name; // Get repo name
    const reqFromBranch = body.pullrequest.source.branch.name; // get source branch name
    const reqToBranch = body.pullrequest.destination.branch.name; // get target branch name
    const reqActionType = req.headers["x-event-key"] ? // get action type that triggered the webhook
      req.headers["x-event-key"].split(':')[1]:
      null;

      triggerControllers.forEach(trigger => {
        const {repoName, fromBranch, toBranch, actionType} = trigger.params;
        if (repoName && reqRepoName !== repoName) return;
        if (fromBranch && !minimatch(reqFromBranch, fromBranch)) return;
        if (toBranch && !minimatch(reqToBranch, toBranch)) return;
        if (actionType && actionType !== "any" && actionType !== reqActionType) return;
        const msg = `${reqRepoName} ${reqFromBranch}->${reqToBranch} ${reqActionType}`;
        trigger.execute(msg, body);
      });
      res.status(200).send("OK");
    }
    catch (err){
      res.status(422).send(err.message);
    }
}

module.exports = {
    webhookPush,
    webhookPR
};

*/

import (
	"encoding/json"
	"fmt"
	"regexp"
	"strings"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

type bitbucketPRData struct {
	repository  bitbucketRepo `json:"repository"`
	pullrequest bitbucketPR   `json:"pullrequest"`
	push        bitbucketPush `json:"push"`
}

type bitbucketRepo struct {
	name string `json:"name"`
}

type bitbucketPR struct {
	source      bitbucketTree `json:"source"`
	destination bitbucketTree `json:"destination"`
}

type bitbucketTree struct {
	branch bitbucketBranch `json:"branch"`
}

type bitbucketBranch struct {
	name string `json:"name"`
}

func bitbucket_trigger_webhook_pr(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	body := &bitbucketPRData{}
	if jerr := json.Unmarshal(triggerInput.Data, &body); jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}

	reqRepoName := body.repository.name                     // Get repo name
	reqFromBranch := body.pullrequest.source.branch.name    // get source branch name
	reqToBranch := body.pullrequest.destination.branch.name // get target branch name
	reqActionType := ""
	if keys, ok := triggerInput.Header["x-event-key"]; ok {
		if len(keys) > 0 {
			parts := strings.Split(keys[0], ":")
			if len(parts) > 1 {
				reqActionType = parts[1]
			}
		}
	}

	count := 0
	for _, t := range triggerInput.Triggers {
		params, err := p.GetTriggerParams(t.Name)
		if err != nil {
			l.Errorf("Error getting %s trigger params: %v", err)
			continue
		}
		if anp, ok := params["bitbucket-trigger/webhook-pr/repo-name"]; ok {
			s := anp.(string)
			if r, rerr := regexp.Compile(s); rerr == nil {
				if !r.MatchString(reqRepoName) {
					continue
				}
			} else {
				l.Errorf("Trigger %s has bad repo-name pattern: %v", rerr)
			}
		}
		if anp, ok := params["bitbucket-trigger/webhook-pr/from-branch"]; ok {
			s := anp.(string)
			if r, rerr := regexp.Compile(s); rerr == nil {
				if !r.MatchString(reqFromBranch) {
					continue
				}
			} else {
				l.Errorf("Trigger %s has bad repo-name pattern: %v", rerr)
			}
		}
		if anp, ok := params["bitbucket-trigger/webhook-pr/to-branch"]; ok {
			s := anp.(string)
			if r, rerr := regexp.Compile(s); rerr == nil {
				if !r.MatchString(reqToBranch) {
					continue
				}
			} else {
				l.Errorf("Trigger %s has bad repo-name pattern: %v", rerr)
			}
		}
		if anp, ok := params["bitbucket-trigger/webhook-pr/action-type"]; ok {
			s := anp.(string)
			if s != "any" && s != reqActionType {
				continue
			}
		}

		count++
		plugin.Publish("trigger", "fired", t.Name, body)
	}
	triggerResult.HasCount = true
	triggerResult.Count = count
	return triggerResult, nil
}
