package main

/*
  Name: github-trigger-webhook-push
  Description: Github Push
  Documentation: Github Push
  Path: kaholo-trigger-github/src/config.json

const { verifyRepoName, verifySignature, isMatch } = require("./helpers");

async function webhookPush(req, res, settings, triggerControllers) {
    try {
        const body = req.body;
        if (!body.ref && body.hook){
            // first request
            return res.status(200).send("OK");
        }
        let [_temp, pushType, ...pushName] = body.ref.split("/");
        // fix push name in case it contains /
        if (Array.isArray(pushName)) pushName = pushName.join("/");
        // get the push type
        if (pushType === "heads") pushType = "branch";
        else if (pushType === "tags") pushType = "tag";
        else {
            return res.status(400).send("Bad Push Type");
        }
        const reqRepoName = body.repository.name; //Github repository name
        const reqSecret = req.headers["x-hub-signature-256"];
        const paramName = `${pushType}Pat`;

        triggerControllers.forEach((trigger) => {
            if (!verifyRepoName(trigger, reqRepoName) || !verifySignature(trigger, reqSecret, body)) return;
            if (trigger.params.tagPat || trigger.params.branchPat){ // If both weren't provided, don't filter by push name
                const validateParam = trigger.params[paramName];
                if (!validateParam || !isMatch(pushName, validateParam)) return;
            }
            const msg = `Github ${reqRepoName} ${pushName} ${pushType} Push`;
            trigger.execute(msg, body);
        });
        res.status(200).send("OK");
    }
    catch (err){
        res.status(422).send(err.toString());
    }
}
*/

import (
	"crypto"
	"crypto/hmac"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"regexp"
	"strings"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

func github_trigger_webhook_push(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	body := &githubRequest{}
	if jerr := json.Unmarshal(triggerInput.Data, &body); jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}

	if body.PullRequest == nil && body.Hook != "" {
		return triggerResult, nil
	}

	reqRepoName := body.Repository.Name //Github repository name
	reqSecret := ""
	reqSecrets := triggerInput.Header["x-hub-signature-256"]
	if len(reqSecrets) > 0 {
		reqSecret = reqSecrets[0]
	}

	parts := strings.SplitN(body.Ref, "/", 3)
	if len(parts) <= 2 {
		triggerResult.Code = 400
		triggerResult.Data = []byte("Bad ref string")
		return triggerResult, nil
	}
	pushType := parts[1]
	pushName := parts[2]
	paramName := ""
	if pushType == "heads" {
		paramName = "github-trigger/webhook-push/branch-pat"
	} else if paramName == "tags" {
		paramName = "github-trigger/webhook-push/tag-pat"
	} else {
		triggerResult.Code = 400
		triggerResult.Data = []byte("Bad push type")
		return triggerResult, nil
	}

	count := 0
	for _, t := range triggerInput.Triggers {
		params, err := p.GetTriggerParams(t.Name)
		if err != nil {
			l.Errorf("Error getting %s trigger params: %v", err)
			continue
		}
		if anp, ok := params["github-trigger/webhook-push/secret"]; ok {
			s := anp.(string)
			if s != "" {
				mac := hmac.New(crypto.SHA256.New, []byte(s))
				mac.Write(triggerInput.Data)
				shaHex := hex.EncodeToString(mac.Sum(nil))
				if shaHex != reqSecret[7:] {
					continue
				}
			}
		}
		if anp, ok := params["github-trigger/webhook-push/repo-name"]; ok {
			s := anp.(string)
			if r, rerr := regexp.Compile(s); rerr == nil {
				if !r.MatchString(reqRepoName) {
					continue
				}
			} else {
				l.Errorf("Trigger %s has bad repo-name pattern: %v", rerr)
			}
		}
		if anp, ok := params[paramName]; ok {
			s := anp.(string)
			if r, rerr := regexp.Compile(s); rerr == nil {
				if !r.MatchString(pushName) {
					continue
				}
			} else {
				l.Errorf("Trigger %s has bad %s pattern: %v", paramName, rerr)
			}
		}
		count++
		plugin.Publish("trigger", "fired", t.Name, body)
	}
	triggerResult.HasCount = true
	triggerResult.Count = count

	return triggerResult, nil
}
