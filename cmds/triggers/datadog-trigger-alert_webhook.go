package main

/*
  Name: datadog-trigger-alert_webhook
  Description: Datadog alert webhook
  Documentation: Datadog alert webhook
  Path: kaholo-trigger-datadog/src/config.json

function alertWebhook(req, res, settings, triggerControllers) {
    try {
        const body = req.body;
        const alertId = body.alertId || body.id;
        triggerControllers.forEach(trigger => {
            const id = trigger.params.ALERT_ID;
            if (id && id != alertId) return;
            trigger.execute(body.title, body);
        });
        res.status(200).send("OK");
    }
    catch (err){
        res.status(422).send(err.message);
    }
}

module.exports = {
    ALERT_WEBHOOK: alertWebhook
}


*/

import (
	"encoding/json"
	"fmt"
	"regexp"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

func datadog_trigger_alert_webhook(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	body := map[string]interface{}{}
	if jerr := json.Unmarshal(triggerInput.Data, &body); jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}
	alertId := ""
	alertIdObj, ok := body["alertId"]
	if !ok {
		alertIdObj, ok = body["id"]
	}
	if !ok {
		triggerResult.Code = 422
		triggerResult.Data = []byte("Missing alertId or id")
		return triggerResult, nil
	}
	alertId = alertIdObj.(string)

	count := 0
	for _, t := range triggerInput.Triggers {
		params, err := p.GetTriggerParams(t.Name)
		if err != nil {
			l.Errorf("Error getting %s trigger params: %v", err)
			continue
		}
		if anp, ok := params["datadog-trigger/alert-webhook/alert-id"]; ok {
			s := anp.(string)
			if r, rerr := regexp.Compile(s); rerr == nil {
				if !r.MatchString(alertId) {
					continue
				}
			} else {
				l.Errorf("Trigger %s has bad repo-name pattern: %v", rerr)
			}
		}

		count++
		plugin.Publish("trigger", "fired", t.Name, body)
	}
	triggerResult.HasCount = true
	triggerResult.Count = count

	return triggerResult, nil
}
