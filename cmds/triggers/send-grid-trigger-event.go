package main

/*
  Name: send-grid-trigger-event
  Description: Email Event
  Documentation: Email Event
  Path: kaholo-trigger-sendgrid/src/config.json

const { isMatch } = require("micromatch");
const { verifyRequest, SIG_HEADER, TIME_HEADER } = require("./helpers");

async function event(req, res, settings, triggerControllers) {
  const {publicKey} = settings;
  try {
    const signature = req.get(SIG_HEADER);
    const timestamp = req.get(TIME_HEADER);
    const body = req.body;
    if (publicKey && (!signature || !timestamp || !body ||
        !verifyRequest(publicKey, body, signature, timestamp))){
      return res.status(403).send("Bad SendGrid event format");
    }
    body.forEach(eventPayload => {
      const {email, event: eventType} = eventPayload;
      let categories = eventPayload.category;
      if (!Array.isArray(categories)) categories = [categories];

      triggerControllers.forEach(trigger => {
          const { emailPattern, trigEventType} = trigger.params;
          const categoryPats = (trigger.params.categoryPats || "").split("\n").filter(val => val);

          if (emailPattern && !isMatch(email, emailPattern)) return;

          if (categoryPats.length > 0 && !categories.some(category =>
            categoryPats.some(categoryPat => isMatch(category, categoryPat)))) return;

          if (trigEventType && trigEventType !== "any" && eventType !== trigEventType) {
            const isDelivery = ["processed", "dropped", "delivered",
                                "deferred", "bounce"].includes(eventType);
            if (trigEventType === "anyDelivery" && !isDelivery) return;
            if (trigEventType === "anyEngagement" && isDelivery) return;
          }

          trigger.execute(`SendGrid email ${eventType} - ${email}`, eventPayload);
      });
    })
    res.status(204).send("OK");
  }
  catch (err){
    res.status(500).send({err: err.message || err});
  }
}

module.exports = {
  event
};

*/

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"regexp"
	"strings"

	"github.com/sendgrid/sendgrid-go/helpers/eventwebhook"
	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

type sendgridRequest struct {
	Events []sendgridEventPayload `json:"eventPayload"`
}

type sendgridEventPayload struct {
	Email     string `json:"email"`
	EventType string `json:"event"`
	Category  string `json:"category"`
}

var eventTypes = []string{"processed", "dropped", "delivered", "deferred", "bounce"}

func send_grid_trigger_event(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {

	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	verificationHeader := ""
	verificationHeaders := triggerInput.Header[eventwebhook.VerificationHTTPHeader]
	if len(verificationHeaders) > 0 {
		verificationHeader = verificationHeaders[0]
	}

	timestampHeader := ""
	timestampHeaders := triggerInput.Header[eventwebhook.TimestampHTTPHeader]
	if len(timestampHeaders) > 0 {
		timestampHeader = timestampHeaders[0]
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	body := &sendgridRequest{}
	if jerr := json.Unmarshal(triggerInput.Data, &body); jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}

	count := 0
	for _, event := range body.Events {
		for _, t := range triggerInput.Triggers {
			params, err := p.GetTriggerParams(t.Name)
			if err != nil {
				l.Errorf("Error getting %s trigger params: %v", err)
				continue
			}

			if val, ok := params["send-grid-trigger/public-key"]; ok {
				s := val.(string)

				if s != "" {
					if verificationHeader != "" || timestampHeader != "" {
						base64Key := base64.StdEncoding.EncodeToString([]byte(s))
						publicKey, _ := eventwebhook.ConvertPublicKeyBase64ToECDSA(base64Key)
						verified, _ := eventwebhook.VerifySignature(publicKey, triggerInput.Data, verificationHeader, timestampHeader)
						if !verified {
							triggerResult.Code = 403
							triggerResult.Data = []byte(fmt.Sprintf("Bad SendGrid event format"))
							return triggerResult, nil
						}
					}
				}
			}

			if val, ok := params["send-grid-trigger/event/email-pattern"]; ok {
				s := val.(string)
				if r, rerr := regexp.Compile(s); rerr == nil {
					if !r.MatchString(event.Email) {
						continue
					}
				} else {
					l.Errorf("Trigger %s has bad email pattern: %v", rerr)
				}
			}

			if val, ok := params["send-grid-trigger/event/trig-event-type"]; ok {
				s := val.(string)

				if s != "" && s != "any" && s != event.EventType {
					isDelivery := false
					for _, item := range eventTypes {
						if event.EventType == item {
							isDelivery = true
						}
					}
					if s == "anyDelivery" && !isDelivery {
						continue
					}
					if s == "anyEngagement" && isDelivery {
						continue
					}
				}
			}

			if val, ok := params["send-grid-trigger/event/category-pats"]; ok {
				s := val.(string)
				if s != "" {
					for _, pat := range strings.Split(s, "\n") {
						if r, rerr := regexp.Compile(pat); rerr == nil {
							if !r.MatchString(event.Category) {
								continue
							}
						} else {
							l.Errorf("Trigger %s has bad button action pattern: %v", rerr)
						}
					}
				}
			}
			count++
			plugin.Publish("trigger", "fired", t.Name, body)
		}
	}
	triggerResult.HasCount = true
	triggerResult.Count = count
	return triggerResult, nil
}
