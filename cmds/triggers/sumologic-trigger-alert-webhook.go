package main

/*
  Name: sumologic-trigger-alert-webhook
  Description: Sumologic alert webhook
  Documentation: Sumologic alert webhook
  Path: kaholo-trigger-sumologic/src/config.json

const minimatch = require("minimatch");

const errMsg = "bad Sumo Logic webhook payload configuration";

function alertWebhook(req, res, settings, triggerControllers) {
    try {
        if (!req.body.hasOwnProperty("kaholo")){
            return res.status(400).send(errMsg);
        }
        const mData = req.body.kaholo;

        const reqName = mData.name; // Get alert name
        const reqType = mData.type; // Get alert type
        const reqQueryName = mData.queryName; // Get the query name that triggered the alert
        const description = mData.description; // Get alert description
        if (!reqName || !reqType || !reqQueryName || !description){
            return res.status(400).send(errMsg);
        }

        triggerControllers.forEach(trigger => {
            const {alertName, alertType, queryName} = trigger.params;
            if (alertName && !minimatch(reqName, alertName)) return;
            if (alertType && alertType !== "any" && alertType !== reqType) return;
            if (queryName && queryName !== reqQueryName) return;

            trigger.execute(`${reqName}: ${description}`, mData);
        });
        res.status(200).send("OK");
    }
    catch (err){
      res.status(422).send(err.message);
    }
}

module.exports = { alertWebhook }


*/

import (
	"encoding/json"
	"fmt"
	"regexp"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

type sumologicRequest struct {
	Alert sumologicRequestBody `json:"rackn"`
}

type sumologicRequestBody struct {
	Name        string `json:"name"`
	Type        string `json:"type"`
	QueryName   string `json:"queryName"`
	Description string `json:"description"`
}

func sumologic_trigger_alert_webhook(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	body := &sumologicRequest{}
	if jerr := json.Unmarshal(triggerInput.Data, &body); jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}

	if body.Alert.Type == "" || body.Alert.Description == "" || body.Alert.Name == "" || body.Alert.QueryName == "" {
		triggerResult.Code = 400
		triggerResult.Data = []byte(fmt.Sprintf("Bad Webhook Format"))
		return triggerResult, nil
	}

	count := 0
	for _, t := range triggerInput.Triggers {
		params, err := p.GetTriggerParams(t.Name)
		if err != nil {
			l.Errorf("Error getting %s trigger params: %v", err)
			continue
		}

		if val, ok := params["sumologic-trigger/alert-webhook/alert-name"]; ok {
			s := val.(string)
			if s != "" {
				if r, rerr := regexp.Compile(s); rerr == nil {
					if !r.MatchString(body.Alert.Name) {
						continue
					}
				} else {
					l.Errorf("Trigger %s has bad alert pattern: %v", s, rerr)
				}
			}
		}

		if val, ok := params["sumologic-trigger/alert-webhook/alert-type"]; ok {
			s := val.(string)
			if s != "" && s != "any" && s != body.Alert.Type {
				continue
			}
		}

		if val, ok := params["sumologic-trigger/alert-webhook/query-name"]; ok {
			s := val.(string)
			if s != "" && s != body.Alert.QueryName {
				continue
			}
		}

		count++
		plugin.Publish("trigger", "fired", t.Name, body)
	}

	triggerResult.HasCount = true
	triggerResult.Count = count
	return triggerResult, nil
}
