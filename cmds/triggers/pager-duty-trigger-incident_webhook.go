package main

/*
  Name: pager-duty-trigger-incident_webhook
  Description: PagerDuty Event
  Documentation: PagerDuty Event
  Path: kaholo-trigger-pagerduty/src/config.json

function alertWebhook(req, res, settings, triggerControllers) {
    try {
        const event = req.body.event;
        const eventType = event.event_type; // Get event type
        const {title, urgency} = event.data;
        if (!eventType || !title || !urgency){
            res.status(400).send("bad pagerduty alert format");
        }
        const msg = `Pager Duty Incident: ${title}`;
        const finishedTrigs = filterTriggers(triggerControllers, eventType, urgency, msg, event);
        res.status(200).send(finishedTrigs);
    }
    catch (err){
        res.status(500).send(err.message);
    }
}

function filterTriggers(triggers, eventType, urgency, msg, body){
    const finishedTrigs = [];
    triggers.forEach(trigger => {
        const trigEventType = trigger.params.EVENT || "any";
        const trigUrgency = trigger.params.urgency || "any";
        if (trigEventType !== "any" && trigEventType !== eventType) return addTrigger(finishedTrigs, trigger, "Event Type");
        if (trigUrgency !== "any" && trigUrgency !== urgency) return addTrigger(finishedTrigs, trigger, "Urgency");
        trigger.execute(msg, body);
        addTrigger(finishedTrigs, trigger);
    });
    return finishedTrigs;
}

function addTrigger(arr, trigger, error){
    if (error) trigger.error = error;
    arr.push(trigger);
}

module.exports = {
    INCIDENT_WEBHOOK: alertWebhook
}

*/

import (
	"bytes"
	"encoding/json"
	"fmt"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

// Event https://developer.pagerduty.com/docs/ZG9jOjQ1MjA5ODc1-overview-v2-webhooks

type pagerDutyRequest struct {
	Event pagerDutyEvent `json:"event"`
}

type pagerDutyEvent struct {
	Type string             `json:"event_type"`
	Data pagerDutyEventData `json:"data"`
}

type pagerDutyEventData struct {
	Title   string `json:"title"`
	Urgency string `json:"urgency"`
}

func pager_duty_trigger_incident_webhook(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {

	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	body := &pagerDutyRequest{}
	if jerr := json.Unmarshal(triggerInput.Data, &body); jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}

	if body.Event.Type == "" || body.Event.Data.Title == "" || body.Event.Data.Urgency == "" {
		triggerResult.Code = 400
		triggerResult.Data = []byte(fmt.Sprintf("Bad pagerduty alert format"))
		return triggerResult, nil
	}

	var finishedTriggers []*models.Trigger
	count := 0
	for _, t := range triggerInput.Triggers {
		params, err := p.GetTriggerParams(t.Name)
		if err != nil {
			l.Errorf("Error getting %s trigger params: %v", err)
			continue
		}
		trigEventType, ok := params["pager-duty-trigger/incident_webhook/event"].(string)
		if !ok {
			trigEventType = "any"
		}
		trigUrgency, ok := params["pager-duty-trigger/incident_webhook/urgency"].(string)
		if !ok {
			trigUrgency = "any"
		}

		if trigEventType != "any" && trigEventType != body.Event.Type {
			t.AddError(fmt.Errorf("Event Type"))
			finishedTriggers = append(finishedTriggers, t)
			buf := &bytes.Buffer{}
			enc := json.NewEncoder(buf)
			enc.Encode(finishedTriggers)
			triggerResult.Data = buf.Bytes()
		}
		if trigUrgency != "any" && trigUrgency != body.Event.Data.Urgency {
			t.AddError(fmt.Errorf("Urgency"))
			finishedTriggers = append(finishedTriggers, t)
			buf := &bytes.Buffer{}
			enc := json.NewEncoder(buf)
			enc.Encode(finishedTriggers)
			triggerResult.Data = buf.Bytes()
		}

		count++
		plugin.Publish("trigger", "fired", t.Name, body)
		finishedTriggers = append(finishedTriggers, t)
	}
	triggerResult.HasCount = true
	triggerResult.Count = count
	buf := &bytes.Buffer{}
	enc := json.NewEncoder(buf)
	enc.Encode(finishedTriggers)
	triggerResult.Data = buf.Bytes()

	return triggerResult, nil
}
