package main

/*
  Name: oci-monitoring-trigger-alert-webhook
  Description: Alert
  Documentation: Alert
  Path: kaholo-trigger-oci-montiring/src/config.json

const minimatch = require("minimatch");
const parsers = require("./parsers");
const fetch = require("node-fetch");

async function alertWebhook(req, res, settings, triggerControllers) {
  try {
    const body = req.body;
    if (body.ConfirmationURL){
      const result = await fetch(body.ConfirmationURL);
      res.send("OK");
      return result;
    }
    const sevirity = parsers.severity(body.severity);
    const name = body.title;
    triggerControllers.forEach((trigger) => {
      let {alertNamePat, minAlertSeverity, maxAlertSeverity} = trigger.params;
      minAlertSeverity = parsers.severity(minAlertSeverity || "info");
      maxAlertSeverity = parsers.severity(maxAlertSeverity || "critical");

      if (alertNamePat && !minimatch(name, alertNamePat)) return;
      if (sevirity < minAlertSeverity || sevirity > maxAlertSeverity) return;
      trigger.execute(name, body);
    });
    res.status(200).send("OK");
  }
  catch (err){
    res.status(422).send(err.message);
  }
}

module.exports = {
  alertWebhook
};

*/

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

type ociRequest struct {
	ConfirmationURL string      `json:"ConfirmationURL"`
	Severity        interface{} `json:"severity"`
	Title           string      `json:"title"`
}

func severityToNumber(severity interface{}) (int, error) {
	if severity == nil {
		return 0, nil
	}
	switch severity.(type) {
	case int:
		return severity.(int), nil
	case string:
		switch severity {
		case "info":
			return 1, nil
		case "warning":
			return 2, nil
		case "error":
			return 3, nil
		case "critical":
			return 4, nil
		}
	}
	return 0, fmt.Errorf("unsupported severity format")
}

func oci_monitoring_trigger_alert_webhook(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	body := &ociRequest{}
	if jerr := json.Unmarshal(triggerInput.Data, &body); jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}

	if body.ConfirmationURL != "" {
		client := &http.Client{}
		req, httpError := http.NewRequest("GET", body.ConfirmationURL, nil)
		if httpError != nil {
			triggerResult.Data = []byte(httpError.Error())
			return triggerResult, nil
		}

		resp, err := client.Do(req)
		if err != nil {
			triggerResult.Data = []byte(err.Error())
			return triggerResult, nil
		}
		if resp.Body != nil {
			defer resp.Body.Close()
		}

		triggerResult.Data, _ = ioutil.ReadAll(resp.Body)
		return triggerResult, nil
	}

	severity, err1 := severityToNumber(body.Severity)
	if err1 != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("invalid severity value provided: %v", err1))
		return triggerResult, nil
	}

	count := 0
	for _, t := range triggerInput.Triggers {
		params, err := p.GetTriggerParams(t.Name)
		if err != nil {
			l.Errorf("Error getting %s trigger params: %v", err)
			continue
		}

		if val, ok := params["oci-monitoring-trigger/alert-webhook/alert-name-pat"]; ok {
			s := val.(string)
			if r, rerr := regexp.Compile(s); rerr == nil {
				if !r.MatchString(body.Title) {
					continue
				}
			} else {
				l.Errorf("Trigger %s has bad alert pattern: %v", s, rerr)
			}
		}

		var maxAlertSeverity int
		var minAlertSeverity int
		minas, ok := params["oci-monitoring-trigger/alert-webhook/min-alert-severity"]
		if !ok {
			minAlertSeverity, _ = severityToNumber("info")
		} else {
			minAlertSeverity, _ = severityToNumber(minas)
		}
		maxas, ok := params["oci-monitoring-trigger/alert-webhook/max-alert-severity"]
		if !ok {
			maxAlertSeverity, _ = severityToNumber("critical")
		} else {
			maxAlertSeverity, _ = severityToNumber(maxas)
		}

		if severity < minAlertSeverity || severity > maxAlertSeverity {
			continue
		}

		count++
		plugin.Publish("trigger", "fired", t.Name, body)
	}
	triggerResult.HasCount = true
	triggerResult.Count = count

	return triggerResult, nil
}
