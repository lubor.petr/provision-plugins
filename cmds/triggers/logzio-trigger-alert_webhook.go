package main

/*
  Name: logzio-trigger-alert_webhook
  Description: Logz.io alert webhook
  Documentation: Logz.io alert webhook
  Path: kaholo-trigger-logzio/src/config.json

function alertWebhook(req, res, settings, triggerControllers) {
    try {
        const body = req.body;
        const {alert_title: reqTitle, alert_severity: reqSeverity} = body;
        if (!reqTitle || !reqSeverity){
            return res.status(400).send("Bad Webhook Format");
        }

        triggerControllers.forEach(trigger => {
            const {ALERT_TITLE: title, SEVERITY: severity} = trigger.params;

            if (title && !minimatch(reqTitle, title)) return;
            if (severity && reqSeverity !== severity) return;

            trigger.execute(reqTitle, body);
        });
        res.status(200).send("OK");
    }
    catch (err){
      res.status(422).send(err.message);
    }
}

module.exports = {
    ALERT_WEBHOOK: alertWebhook
}

https://docs.logz.io/api/#operation/TriggeredAlerts

*/

import (
	"encoding/json"
	"fmt"
	"regexp"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

type logzioRequest struct {
	AlertTitle    string `json:"alert_title"`
	AlertSeverity string `json:"alert_severity"`
}

func logzio_trigger_alert_webhook(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {

	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	body := &logzioRequest{}
	if jerr := json.Unmarshal(triggerInput.Data, &body); jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}

	if body.AlertSeverity == "" || body.AlertTitle == "" {
		triggerResult.Code = 400
		triggerResult.Data = []byte(fmt.Sprintf("Bad Webhook Format"))
		return triggerResult, nil
	}

	count := 0
	for _, t := range triggerInput.Triggers {
		params, err := p.GetTriggerParams(t.Name)
		if err != nil {
			l.Errorf("Error getting %s trigger params: %v", err)
			continue
		}

		if val, ok := params["logzio-trigger/alert_webhook/alert_title"]; ok {
			s := val.(string)
			if r, rerr := regexp.Compile(s); rerr == nil {
				if !r.MatchString(body.AlertTitle) {
					continue
				}
			} else {
				l.Errorf("Trigger %s has bad alert title pattern: %v", rerr)
			}
		}

		if val, ok := params["logzio-trigger/alert_webhook/severity"]; ok {
			s := val.(string)
			if s != "" && s != body.AlertSeverity {
				continue
			}
		}

		count++
		plugin.Publish("trigger", "fired", t.Name, body)
	}
	triggerResult.HasCount = true
	triggerResult.Count = count

	return triggerResult, nil
}
