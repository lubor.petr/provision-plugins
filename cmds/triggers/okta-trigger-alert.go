package main

/*
  Name: okta-trigger-alert
  Description: Catch Event
  Documentation: Catch Event
  Path: kaholo-trigger-okta/src/config.json

const micromatch = require("micromatch");
const parsers = require("./parsers");

async function alert(req, res, settings, triggerControllers) {
  try {
    const events = req.body.data.events;
    events.forEach(event => {
      const eventType = event.eventType;
      const severity = parsers.severity(event.severity);
      triggerControllers.forEach((trigger) => {
        let {alertEventTypes, alertSeverity, includeHigherSeverity} = trigger.params;
        alertEventTypes = alertEventTypes ? alertEventTypes.split("\n").map(eType => eType.trim()) : undefined;
        alertSeverity = parsers.severity(alertSeverity);

        if (alertEventTypes && !alertEventTypes.some(eType => micromatch.isMatch(eventType, eType))) return;
        if (alertSeverity !== "Any"){
          if (alertSeverity > severity) return;
          if (!includeHigherSeverity && alertSeverity != severity) return;
        }
        trigger.execute(`Okta Event: ${eventType} ${event.displayMessage}`, event);
      });
    });
    res.status(200).send("OK");
  }
  catch (err){
    res.status(422).send(err.message || JSON.stringify(err));
  }
}

async function verify(req, res, settings, triggerControllers) {
  try {
    const reqChanllenge = req.headers['x-okta-verification-challenge'];
    if (reqChanllenge){
      res.status(200).send({"verification" : reqChanllenge});
      triggerControllers.forEach((trigger) => {
        trigger.execute(`Okta Verified Event Hook`, req.body || {});
      });
      return "Okta Event Hook Verified";
    }
    res.status(422).send("Unknown Request Format");
  }
  catch (err){
    res.status(422).send(err.message || JSON.stringify(err));
  }
}

module.exports = {
  alert,
  verify
};

*/

import (
	"encoding/json"
	"fmt"
	"regexp"
	"strings"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

type oktaRequest struct {
	Events []oktaEvent `json:"events"`
}

type oktaEvent struct {
	EventType string `json:"eventType"`
	Severity  int    `json:"severity"`
}

var oktaSeverityMap = map[string]interface{}{
	"Any":   "Any",
	"DEBUG": 0,
	"INFO":  1,
	"WARN":  2,
	"ERROR": 3,
	"FATAL": 4,
}

func okta_trigger_alert(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	body := &oktaRequest{}
	if jerr := json.Unmarshal(triggerInput.Data, &body); jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}

	count := 0
	for _, event := range body.Events {
		for _, t := range triggerInput.Triggers {
			params, err := p.GetTriggerParams(t.Name)
			if err != nil {
				l.Errorf("Error getting %s trigger params: %v", err)
				continue
			}

			if val, ok := params["okta-trigger/alert/alert-event-types"]; ok {
				s := val.(string)
				isOneAMatch := false
				for _, eType := range strings.Split(s, "\n") {
					if r, rerr := regexp.Compile(strings.TrimSpace(eType)); rerr == nil {
						if r.MatchString(event.EventType) {
							isOneAMatch = true
						}
					} else {
						l.Errorf("Trigger %s has bad from branch pattern: %v", s, rerr)
					}
				}

				if !isOneAMatch {
					continue
				}
			}

			includeHigherSeverity := false
			if val, ok := params["okta-trigger/alert/include-higher-severity"].(bool); ok {
				includeHigherSeverity = val
			}

			if val, ok := params["okta-trigger/alert/alert-severity"]; ok {
				switch v := val.(type) {
				case int:
					if v > event.Severity {
						continue
					}
				case string:
					s := oktaSeverityMap[strings.ToUpper(v)]
					if s != "Any" {
						if !includeHigherSeverity && s != event.Severity {
							continue
						}
					}
				default:
					triggerResult.Code = 422
					triggerResult.Data = []byte(fmt.Sprintf("Unsupported severity format %v", v))
					return triggerResult, nil
				}
			}
			count++
			plugin.Publish("trigger", "fired", t.Name, body)
		}
	}
	triggerResult.HasCount = true
	triggerResult.Count = count
	return triggerResult, nil
}
