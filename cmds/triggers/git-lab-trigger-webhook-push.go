package main

/*
  Name: git-lab-trigger-webhook-push
  Description: GitLab Webhook push
  Documentation: GitLab Webhook push
  Path: kaholo-trigger-gitlab/src/config.json

function webhookPush(req, res, settings, triggerControllers) {
    try {
        const body = req.body;
        if(!body.repository) {
            return res.status(400).send("Repository not found");
        }
        const reqUrl = body.repository.git_http_url; //Gitlab Http URL
        const reqBranch = body.ref.slice(11);
        const reqSecret = req.get('X-Gitlab-Token');

        triggerControllers.forEach((trigger) => {
            const { SECRET: trigSecret, PUSH_BRANCH: branch, REPO_URL: repoUrl } = trigger.params;
            const secret = trigSecret || settings.SECRET;

            if (secret && reqSecret !== secret) return;
            if (branch && !minimatch(reqBranch, branch)) return;
            if (repoUrl && !minimatch(reqUrl, repoUrl)) return;

            const msg = `${reqBranch} Branch Push`;
            trigger.execute(msg, body);
        });
        res.status(200).send("OK");
    }
    catch (err){
        res.status(422).send(err.toString());
    }
}
*/

import (
	"encoding/json"
	"fmt"
	"regexp"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

func git_lab_trigger_webhook_push(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	body := &gitlabRequest{}
	if jerr := json.Unmarshal(triggerInput.Data, &body); jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}

	reqURL := body.Repository.GitHttpUrl
	reqBranch := body.Ref
	reqSecret := ""
	reqSecrets := triggerInput.Header["X-Gitlab-Token"]
	if len(reqSecrets) > 0 {
		reqSecret = reqSecrets[0]
	}

	count := 0
	for _, t := range triggerInput.Triggers {
		params, err := p.GetTriggerParams(t.Name)
		if err != nil {
			l.Errorf("Error getting %s trigger params: %v", err)
			continue
		}
		if anp, ok := params["git-lab-trigger/webhook-push/secret"]; ok {
			s := anp.(string)
			if s == "" {
				if anp, ok := params["git-lab-trigger/secret"]; ok {
					s = anp.(string)
				}
			}
			if s != "" && s != reqSecret {
				continue
			}
		}
		if anp, ok := params["git-lab-trigger/webhook-push/repo_url"]; ok {
			s := anp.(string)
			if r, rerr := regexp.Compile(s); rerr == nil {
				if !r.MatchString(reqURL) {
					continue
				}
			} else {
				l.Errorf("Trigger %s has bad repo_url pattern: %v", rerr)
			}
		}
		if anp, ok := params["git-lab-trigger/webhook-push/push_branch"]; ok {
			s := anp.(string)
			if r, rerr := regexp.Compile(s); rerr == nil {
				if !r.MatchString(reqBranch) {
					continue
				}
			} else {
				l.Errorf("Trigger %s has bad to_branch pattern: %v", rerr)
			}
		}

		count++
		plugin.Publish("trigger", "fired", t.Name, body)
	}
	triggerResult.HasCount = true
	triggerResult.Count = count

	return triggerResult, nil
}
