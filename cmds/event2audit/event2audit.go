package main

//go:generate sh -c "cd content ; drpcli contents bundle ../content.yaml Version=$version"
//go:generate sh -c "drpcli contents document content.yaml > event2audit.rst"

import (
	_ "embed"
	"fmt"
	"os"
	"strings"
	"sync"

	utils2 "github.com/VictorLowther/jsonpatch2/utils"

	"gitlab.com/rackn/logger"
	v4 "gitlab.com/rackn/provision-plugins/v4"
	"gitlab.com/rackn/provision-plugins/v4/utils"
	"gitlab.com/rackn/provision/v4/api"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

var (
	//go:embed content.yaml
	content string
	version = v4.RSVersion
	def     = models.PluginProvider{
		Name:          "event2audit",
		Version:       version,
		PluginVersion: 4,
		AutoStart:     true,
		RequiredParams: []string{
			"event2audit/events",
		},
		Content: content,
	}
)

type Plugin struct {
	logs   chan *models.Event
	cm     *sync.RWMutex
	events []string
}

func (p *Plugin) SelectEvents() []string {
	return p.events
}

func (p *Plugin) writer(l logger.Logger) {
	for buf := range p.logs {
		l.Auditf("principal %s triggered event %s on %s:%s at %s", buf.Principal, buf.Action, buf.Type, buf.Key, buf.Time)
	}
}

func (p *Plugin) Config(l logger.Logger, session *api.Client, config map[string]interface{}) *models.Error {
	p.cm.Lock()
	defer p.cm.Unlock()
	close(p.logs)
	p.events = nil

	events := []string{}
	eventsRaw, ok := config["event2audit/events"]
	if !ok {
		return utils.ConvertError(500, fmt.Errorf("event2audit/events not specified"))
	}
	if merr := utils2.Remarshal(eventsRaw, &events); merr != nil {
		return utils.ConvertError(500, fmt.Errorf("event2audit/events not valid: %v", merr))
	}
	seen := map[string]struct{}{}
	for _, k := range events {
		if _, ok := seen[k]; ok {
			continue
		}
		seen[k] = struct{}{}
		arr := strings.SplitN(k, ".", 3)
		if len(arr) != 3 {
			l.NoRepublish().Errorf("Invalid event %s: not in scope.action.specific form", k)
			continue
		}
		if arr[0] == "*" || arr[1] == "*" {
			l.NoRepublish().Errorf("Events specifying all scopes or all actions are not allowed, lest you drown in a sea of irrelevant audit logs. Ignoring %s", k)
			continue
		}
		if arr[0] == "log" || arr[0] == "logs" {
			l.NoRepublish().Errorf("Auditing all log events will lead to the swift and inevitable demise of event2audit and dr-provision.  Ignoring %s", k)
			continue
		}
		p.events = append(p.events, k)
	}

	p.logs = make(chan *models.Event, 16)
	go p.writer(l)
	return nil
}

func (p *Plugin) Publish(l logger.Logger, e *models.Event) (err *models.Error) {
	p.cm.RLock()
	if len(p.logs) == cap(p.logs) {
		p.cm.RUnlock()
		l.NoRepublish().Errorf("Exceeded 16 queued log writes, dropping event.")
		return nil
	}
	p.logs <- e
	p.cm.RUnlock()
	return nil
}

func (p *Plugin) Stop(l logger.Logger) {
	p.cm.Lock()
	close(p.logs)
}

func main() {
	p := &Plugin{
		logs: make(chan *models.Event, 16),
		cm:   &sync.RWMutex{},
	}
	plugin.InitApp("event2audit", "Translates events into audit logs", version, &def, p)
	err := plugin.App.Execute()
	if err != nil {
		os.Exit(1)
	}
}
