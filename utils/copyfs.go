package utils

import (
	"archive/zip"
	"bytes"
	"github.com/klauspost/compress/zstd"
	"io"
	"io/fs"
	"os"
	"path"
)

func Unpack(src []byte, target string) error {
	buf := bytes.NewReader(src)
	realFS, err := zip.NewReader(buf, int64(len(src)))
	if err != nil {
		panic(err)
	}
	realFS.RegisterDecompressor(zstd.ZipMethodWinZip, zstd.ZipDecompressor())
	tgtFS := os.DirFS(target)
	err = fs.WalkDir(realFS, ".", func(p string, st fs.DirEntry, err error) error {
		tgtName := path.Join(target, p)
		tst, trr := os.Stat(tgtName)
		sst, srr := st.Info()
		if srr != nil {
			return srr
		}
		if !sst.Mode().IsRegular() {
			return nil
		}
		if trr == nil &&
			tst.ModTime() == sst.ModTime() &&
			tst.Mode() == sst.Mode() &&
			sst.Size() == tst.Size() {
			return nil
		}
		tmpName := tgtName + ".tmp"
		defer os.Remove(tmpName)
		if dErr := os.MkdirAll(path.Dir(tmpName), 0755); dErr != nil {
			return dErr
		}
		tgt, trr := os.Create(tmpName)
		if tgt != nil {
			defer tgt.Close()
		}
		if trr != nil {
			return trr
		}
		src, srr := realFS.Open(p)
		if src != nil {
			defer src.Close()
		}
		if srr != nil {
			return srr
		}
		_, srr = io.Copy(tgt, src)
		if srr != nil {
			return srr
		}
		if srr = os.Rename(tmpName, tgtName); srr != nil {
			return srr
		}
		if srr = os.Chmod(tgtName, sst.Mode()); srr != nil {
			return srr
		}
		return os.Chtimes(tgtName, sst.ModTime(), sst.ModTime())
	})
	if err != nil {
		return err
	}
	return fs.WalkDir(tgtFS, ".", func(p string, st fs.DirEntry, err error) error {
		if _, se := fs.Stat(realFS, p); se == nil {
			return nil
		}
		targetName := path.Join(target, p)
		if st.IsDir() {
			return nil
		}
		if st.Type().Perm()&0200 == 0 {
			if srr := os.Chmod(targetName, st.Type().Perm()|0200); srr != nil {
				return srr
			}
		}
		os.Remove(targetName)
		return nil
	})
}
